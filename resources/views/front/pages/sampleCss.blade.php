<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="application-name" content="Codi Music">
    <meta name="keywords" content="codi music">
    <meta name="author" content="Vu Dinh Ba">
    <meta name="description" content="Best beautiful web which i have coded">

    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css">

    <script type="application/javascript" src="/bootstrap/js/jquery.min.js"></script>
    <script type="application/javascript" src="/bootstrap/js/bootstrap.min.js"></script>

    <title>Hoc Bootstrap</title>
</head>
<body>
<div class="container">
    <hr>
    <!-- form normal -->
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form action="">
                <div class="form-group has-feedback has-success">
                    <label for="input1">Họ tên :</label>
                    <input type="text" class="form-control" placeholder="Họ tên" id="input1"
                           aria-describedby="true">
                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                    <span class="help-block">Gia tri hop le</span>
                </div>
                <div class="form-group has-error has-feedback">
                    <label for="input2">Email : </label>
                    <input type="text" class="form-control" placeholder="email" id="input2"
                           aria-describedby="true">
                    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                    <span class="help-block">Gia tri khong hop le</span>
                </div>
                <div class="form-group has-warning has-feedback">
                    <label for="input3">Mật khẩu : </label>
                    <input type="password" name="password" id="input3" class="form-control"
                           aria-describedby="true">
                    <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>
                    <span class="help-block">Hay can trong voi gia tri nay</span>
                </div>
                <div class="form-group">
                    <label for="input4">Mật khẩu xác nhận : </label>
                    <input type="password" name="confirm_password" id="input4" class="form-control">
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label for="inputPictrue">Ảnh đại diện</label>
                    <input type="file" id="inputPictrue" name="picture">
                    <em>Hãy chọn ảnh đại diện chất lượng tốt.</em>
                </div>
                <div class="checkbox">
                    <label for="input5">
                        <input type="checkbox" id="input5"> Chấp nhận các điều khoản cam kết
                    </label>
                </div>
                <hr>
                <div class="text-center">
                    <button class="btn btn-primary">Tạo mới</button>
                    <button class="btn btn-default">Hủy bỏ</button>
                </div>
            </form>
        </div>
    </div>
    <!-- End form -->
    <hr>
    <!-- form inline -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="" class="form-inline">
                <div class="form-group">
                    <label for="input1">Họ tên :</label>
                    <input type="text" class="form-control" placeholder="Họ tên" id="input1">
                </div>
                <div class="form-group">
                    <label for="input2">Email : </label>
                    <input type="text" class="form-control" placeholder="email" id="input2">
                </div>
                <button class="btn btn-primary">Tạo mới</button>

            </form>
        </div>
    </div>
    <!-- End form -->
    <hr>
    <!-- form inline no label-->

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="" class="form-inline">
                <div class="form-group">
                    <label for="input1" class="sr-only">Họ tên :</label>
                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        <input type="text" class="form-control" placeholder="Họ tên" id="input1">
                        <div class="input-group-addon">.00</div>
                    </div>
                </div>
                <button class="btn btn-primary">Chuyển đồi tiền</button>

            </form>
        </div>
    </div>
    <!-- End form -->
    <hr>
    <!-- form inline no label-->
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="" class="form-horizontal">
                <div class="form-group has-error">
                    <label for="input1" class="col-sm-3">Họ tên :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="Họ tên" id="input1">
                        <span class="help-block">Co mot loi xay ra trong qua trinh lay du lieu</span>
                    </div>
                </div>
                <div class="form-group has-success">
                    <label for="input2" class="col-sm-3">Email : </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="email" id="input2">
                        <span class="help-block">Co mot loi xay ra trong qua trinh lay du lieu</span>
                    </div>
                </div>
                <div class="form-group has-warning">
                    <label for="input3" class="col-sm-3">Mật khẩu : </label>
                    <div class="col-sm-9">
                        <input type="password" name="password" id="input3" class="form-control">
                        <span class="help-block">Co mot loi xay ra trong qua trinh lay du lieu</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="input4" class="col-sm-3">Nhập lại mật khẩu : </label>
                    <div class="col-sm-9">
                        <input type="password" name="confirm_password" id="input4" class="form-control">
                        <span class="help-block">Co mot loi xay ra trong qua trinh lay du lieu</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <div class="checkbox has-error">
                            <label for="input5">
                                <input type="checkbox" id="input5"> Chấp nhận
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-3">
                        <button class="btn btn-primary">Tạo mới</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- End form -->
    <hr>
    <!-- form mo ta-->

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="" class="">
                <div class="form-group">
                    <label for="input1">Họ tên :</label>
                    <input type="text" class="form-control" placeholder="Họ tên" id="input1">

                </div>
                <div class="form-group">
                    <label for="input2" >Mô tả : </label>
                    <textarea name="descripton" id="input2" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Tạo mới</button>
                </div>
            </form>
        </div>
    </div>
    <!-- End form -->
    <hr>
    <!-- form mo ta-->

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="" class="">
                <div class="form-group">
                    <label for="input1">Họ tên :</label>
                    <input type="text" class="form-control" placeholder="Họ tên" id="input1">
                </div>
                <div class="form-group">
                    <label for="input2" >Mô tả : </label>
                    <select name="chonlua" id="input2" class="form-control" multiple >
                        <option value="1">Qua vai</option>
                        <option value="2">Qua chanh</option>
                        <option value="3">Qua dua</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Tạo mới</button>
                </div>
            </form>
        </div>
    </div>
    <!-- End form -->
    <hr>
    <!-- lam viec voi .sr-only  hide label-->

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="" class="">
                <div class="form-group has-feedback">
                    <label for="input1">Họ tên :</label>
                    <input type="text" class="form-control" placeholder="Họ tên" id="input1"
                           aria-describedby="true">
                    <span class="form-control-feedback glyphicon glyphicon-ok" aria-hidden="true"></span>

                </div>
                <div class="form-group has-feedback">
                    <label for="input1" class="">Họ tên :</label>
                    <input type="text" class="form-control input-lg" placeholder="Họ tên" id="input1"
                           aria-describedby="true">
                    <span class="form-control-feedback glyphicon glyphicon-ok" aria-hidden="true"></span>
                </div>
                <div class="form-group has-feedback">
                    <label for="input1" class="">Họ tên :</label>
                    <input type="text" class="form-control input-sm" placeholder="Họ tên" id="input1"
                           aria-describedby="true">
                    <span class="form-control-feedback glyphicon glyphicon-ok" aria-hidden="true"></span>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block disabled">Tạo mới</button>
                </div>
            </form>
        </div>
    </div>
    <!-- End form -->
    <hr>
    <!-- IMAGE test kieu web quang cao-->
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3 col-xs-12">
                <img src="https://i.ytimg.com/vi/_rdtQvaYNWI/maxresdefault.jpg" class="img-responsive"
                     alt="responsive image">
                <br>
                <img src="https://i.ytimg.com/vi/JGDqm-Bdll4/maxresdefault.jpg" class="img-responsive"
                     alt="responsive image">
                <br>
                <img src="https://i.ytimg.com/vi/_rdtQvaYNWI/maxresdefault.jpg" class="img-responsive"
                     alt="responsive image">
            </div>
            <div class="col-md-6 col-xs-12">
                <!-- Post Content -->
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut,
                    error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae
                    laborum minus inventore?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste
                    ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus,
                    voluptatibus.</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde
                    eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis.
                    Enim, iure!</p>

                <blockquote class="blockquote">
                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a
                        ante.</p>
                    <footer class="blockquote-footer">Someone famous in
                        <cite title="Source Title">Source Title</cite>
                    </footer>
                </blockquote>
                <div class="col-md-12">
                    <p class="bg-dark">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <p class="bg-danger">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <p class="bg-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <p class="bg-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <p class="bg-success">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

                </div>

            </div>

            <div class="col-md-3 col-xs-12">
                <img src="https://i.ytimg.com/vi/_rdtQvaYNWI/maxresdefault.jpg" class="img-responsive"
                     alt="responsive image">
                <br>
                <img src="https://i.ytimg.com/vi/JGDqm-Bdll4/maxresdefault.jpg" class="img-responsive"
                     alt="responsive image">
                <br>
                <img src="https://i.ytimg.com/vi/_rdtQvaYNWI/maxresdefault.jpg" class="img-responsive"
                     alt="responsive image">

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- End div -->
    <hr>

    <!-- IMAGE test kieu web quang cao-->
    <div class="row">
        <div class="col-md-12">
            <div style="height: 300px" class="bg-success">
                <br>
                <br>
                <p class="bg-primary">
                    <button class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Ngon thi vay day</p>
                <span class="caret"></span>

            </div>

        </div>
    </div>
    <!-- End div -->

</div>
</body>
</html>