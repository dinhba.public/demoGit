@extends('layouts.admin')

@section('title', 'This is a blank page')
@section('description', 'This is a blank page that needs to be implemented')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin <small>Danh sách thành viên</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Danh sách thành viên
                    </li>
                </ol>
                @include('notifications.status_message')
                @include('notifications.errors_message')
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">

                <table class="table">
                    <thead>
                        <td>STT</td>
                        <td>Họ tên</td>
                        <td>Tài khoản</td>
                        <td>Email</td>
                    </thead>
                    <tbody>
                        @foreach($result as $i => $member)
                            <tr>
                                <td>{{$i + 1}}</td>
                                <td>{{$member['first_name'] . ' ' . $member['last_name']}}</td>
                                <td>{{$member['username']}}</td>
                                <td>{{$member['email']}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
@endsection