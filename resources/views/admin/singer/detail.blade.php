@extends('layouts.admin')

@section('title', 'Thông tin chi tiết')
@section('description', 'This is a blank page that needs to be implemented')

@section('content')
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin <small>Ca sĩ, diễn viên</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Thông tin chi tiết
                    </li>
                </ol>
                @include('notifications.status_message')
                @include('notifications.errors_message')
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-8">
                <div class="col-md-4 col-xs-12">
                    <div >
                        <img src="https://kenh14cdn.com/ba54950424/2015/07/24/mylinh-2eb80.jpg" class="img-thumbnail">
                    </div>

                </div>
                <div class="col-md-8 col-xs-12">
                    <dl class="dl-horizontal">
                        <dt><code>Họ tên: </code></dt>
                        <dd>Trần Mỹ Linh</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt><code>Năm sinh: </code></dt>
                        <dd>1979</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt><code>Địa chỉ: </code></dt>
                        <dd>Sơn Tây, Hà Nội</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt><code>Lĩnh vực: </code></dt>
                        <dd>Nhạc trẻ, nhạc nhẹ</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt><code>Quá trình: </code></dt>
                        <dd class="text-justify">Sinh ra trong một gia đình lao động ở Hà Nội. Bố từng là giáo viên dạy văn sau chuyển
                            nghề công nhân, thấu xây dựng. Mẹ là công nhân xí nghiệp dược phẩm TW2.
                            Gia đình có 3 anh em, anh trai là kỹ sư về đầu máy diegen, em gái học kinh tế.
                            Mỹ Linh biết hát từ lúc còn rất nhỏ.
                            Lần đầu bước lên sân khấu khi mới lên năm tuổi hát bài “Be bé bằng bông”.
                            Khi còn nhỏ, Mỹ Linh bộc lộ nhiều khả năng khác nhau, có thể hát, múa hay đóng kịch
                            và kể chuyện rất cuốn hút và đã được rất nhiều các giải thưởng cá nhân về ca hát
                            măng non hay thi kể chuyện măng non tại địa phương . Mỹ Linh học phổ thông
                            cơ sở ( từ lớp 1 đến lớp 8 ) tại PTCS Trưng Nhị, quận Hai Bà Trưng Hà Nội.
                            Học PTTH tại trường PTTH Bạch Mai, quận Hai Bà Trưng Hà Nội.
                            Trong 12 năm Mỹ Linh luôn làm quản ca của lớp và của trường nơi mình học.
                            Khi còn đi học Mỹ Linh học tương đối khá đã từng 2 lần được đi dự Đại hội học sinh
                            giỏi thủ đô, được thủ tướng khen thưởng. Khi còn đi học Mỹ Linh đặc biệt có năng khiếu
                            cá môn xã hội như: Văn, Sử và Nhạc.</dd>
                    </dl>





                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
@endsection