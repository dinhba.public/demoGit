@extends('layouts.admin')

@section('title', 'List Singers')
@section('description', 'This is a blank page that needs to be implemented')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin <small>Ảnh</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i>Danh sách ảnh
                    </li>
                </ol>
                @include('notifications.status_message')
                @include('notifications.errors_message')
            </div>
        </div>
        <!-- /.row -->
        <button class="btn btn-primary">Thêm ca sĩ</button>
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <thead>
                    <td>STT</td>
                    <td>Name</td>
                    <td>Link</td>
                    <td>Sub_id</td>
                    <td>Sub_type</td>
                    </thead>
                    <tbody>
                    @foreach($result as $i => $member)
                        <tr>
                            <td>{{$i + 1}}</td>
                            <td>{{$member['name']}}</td>
                            <td>{{$member['link']}}</td>
                            <td>{{$member['sub_id']}}</td>
                            <td>{{$member['sub_type']}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
@endsection