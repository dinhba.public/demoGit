@extends('layouts.admin')

@section('title', 'Thông tin chi tiết')
@section('description', 'This is a blank page that needs to be implemented')

@section('content')
    <link href="/plugins/jplayer/skin/blue.monday/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/plugins/jplayer/lib/jquery.min.js"></script>
    <script type="text/javascript" src="/plugins/jplayer/dist/jplayer/jquery.jplayer.min.js"></script>
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin <small>Bài hát</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Nghe thử
                    </li>
                </ol>
                @include('notifications.status_message')
                @include('notifications.errors_message')
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            @foreach($result as $i => $song)
                <div class="col-md-8">
                    <div class="col-md-4 col-xs-12">
                        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                        <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
                            <div class="jp-type-single">
                                <div class="jp-gui jp-interface">
                                    <div class="jp-controls">
                                        <button class="jp-play" role="button" tabindex="0">play</button>
                                        <button class="jp-stop" role="button" tabindex="0">stop</button>
                                    </div>
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar"></div>
                                        </div>
                                    </div>
                                    <div class="jp-volume-controls">
                                        <button class="jp-mute" role="button" tabindex="0">mute</button>
                                        <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                        <div class="jp-volume-bar">
                                            <div class="jp-volume-bar-value"></div>
                                        </div>
                                    </div>
                                    <div class="jp-time-holder">
                                        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                        <div class="jp-toggles">
                                            <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="jp-details">
                                    <div class="jp-title" aria-label="title">&nbsp;</div>
                                </div>
                                <div class="jp-no-solution">
                                    <span>Update Required</span>
                                    To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-8 col-xs-12">
                        <dl class="dl-horizontal">
                            <dt><code>Tên bài:  </code></dt>
                            <dd><code><strong>{{$song['name']}}</strong></code></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt><code>Nhạc sĩ: </code></dt>
                            <dd><code>{{$song['composer']['realName']}}</code></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt><code>Thể loại: </code></dt>
                            <dd><code>{{$song['musicType']['name']}}</code></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt><code>Album: </code></dt>
                            <dd><code>{{$song['album']['name']}}</code></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt><code>Thông tin: </code></dt>
                            <dd class="text-justify"><code>
                                    {{$song['description']}}
                                </code></dd>
                        </dl>

                    </div>
                </div>
            @endforeach
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <script>
        $(document).ready(function(){

            $("#jquery_jplayer_1").jPlayer({
                ready: function (event) {
                    $(this).jPlayer("setMedia", {
                        title: "Bubble",
                        m4a: "/plugins/emva.mp3",
                        oga: "http://jplayer.org/audio/ogg/Miaow-07-Bubble.ogg"
                    });
                },
                swfPath: "plugins/jplayer/dist/jplayer",
                supplied: "m4a, oga",
                wmode: "window",
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: true,
                keyEnabled: true,
                remainingDuration: true,
                toggleDuration: true
            });
        });
    </script>

@endsection