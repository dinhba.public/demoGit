/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config) {
    // Define changes to default configuration here. For example:
    config.language = 'en';
    config.uiColor = '#c4dcd1';

    config.extraPlugins = 'timestamp';
    config.removePlugins = 'link';
    config.allowedContent = true;
    config.contentsCss = "http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css";
    // config.contentsCss = "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";

    // config.extraAllowedContent = 'a span';
    // editor.addContentsCss( pluginDirectory + 'styles/example.css' );

    // config.toolbar = [{
    //     name: 'clipboard',
    //     items: ['Cut', 'Copy', 'Paste']
    // }, {
    //     name: 'editing',
    //     items: ['Scayt']
    // }, {
    //     name: 'link',
    //     items: ['Link', 'Unlink', 'Anchor']
    // }, {
    //     name: 'timestamp',
    //     items: ['Timestamp']
    // }, {
    //     name: 'basicstyles',
    //     items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat']
    // }
    // ];
};

