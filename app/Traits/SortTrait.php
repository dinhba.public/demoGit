<?php
namespace ckeditor\Traits;

trait SortTrait {

    /**
     * @return Thuat toan sap xep Bubble, O(n^2);
     *
     * Duyet 2 phan tu gan nhau, so sanh roi dao vi tri theo thu tu.
     * Cu the duyet tiep cho den het chuoi
     * Do phuc tap la O(n^2)
     */
    public function bubbleSort ($arr) {
        $n  = count($arr);
        for ($i = 0; $i < $n; $i++) {
            for ($j = $n-1; $j > $i; $j--) {
                if ($arr[$j] < $arr[$j-1]) {
                    $tmp = $arr[$j];
                    $arr[$j] = $arr[$j-1];
                    $arr[$j-1] = $tmp;
                }
            }
        }
        return $arr;
    }

    /**
     * @return Thuat toan sap xep Insertion, O(n) -> O(n^2);
     *
     * Mo hinh sap xep giong xep quan bai,
     * Sap quan bai voi day dau tien, tim vi tri thich hop de nhet quan bai vao
     * Do phuc tap la O(n) -> O(n^2);
     */
    public function insertionSort ($arr1) {
        $n  = count($arr1);
        for ($i = 0; $i < $n; $i++) {
            $tmp = $arr1[$i];

            $j = $i - 1;
            while ($j >= 0 && $arr1[$j] > $tmp) {
                $arr1[$j+1] = $arr1[$j];
                $j--;
            }

            $arr1[$j+1] = $tmp;
        }
        return $arr1;
    }

    /**
     * @return Thuat toan sap xep SelectionSort, O(n) -> O(n^2);
     *
     *  Sap xep chon, Duyet het phan tu, thay thang nao nho nhat
     *  trong mang thi vut len dau bang
     *  Do phuc tap la O(n) -> O(n^2);
     */
    public function selectionSort ($arr) {
        $n  = count($arr);
        for ($i = 0; $i < $n; $i++) {
            $t = $i;
            for ($j = $t; $j < $n; $j++) {
                if ($arr[$t] > $arr[$j]) {
                    $t = $j;
                }
            }

            $tmp = $arr[$i];
            $arr[$i] = $arr[$t];
            $arr[$t] = $tmp;
        }
        return $arr;
    }


    /**
     * @return Thuat toan sap xep Quicksort
     *
     *  Sap xep nhanh nhat : Chon mot phan tu trong mang lam chot,
     *  dua nhung phan tu nho va lon hon chot sang mot ben khac nhau
     *  Cu the de qui cho het mang.
     *  Do phuc tap la O(n*log(n));
     */
    public function quickSort($array) {
        // find array size
        $length = count($array);

        // base case test, if array of length 0 then just return array to caller
        if($length <= 1){
            return $array;
        }
        else{

            // select an item to act as our pivot point, since list is unsorted first position is easiest
            $pivot = $array[0];
            // declare our two arrays to act as partitions
            $left = $right = array();

            // loop and compare each item in the array to the pivot value, place item in appropriate partition
            for($i = 1; $i < count($array); $i++)
            {
                if($array[$i] < $pivot){
                    $left[] = $array[$i];
                }
                else{
                    $right[] = $array[$i];
                }
            }
            // use recursion to now sort the left and right lists

            return array_merge($this->quickSort($left), array($pivot), $this->quickSort($right));
        }

    }

    public function addTwoDigits($n) {
        $firstDigit = (int)($n/10);
        $secondDigit = $n % 10;
        return $firstDigit + $secondDigit;
    }

    /**
     * Kiem tra mang:
     * Kiem tra gia tri dau vao co phai la mot mang so khong
     * Neu cac phan tu trong mang khong phai dang so nguyen
     * thi dua canh bao cho nguoi dung
     *
     * @param $arr
     * @output true or false
     */
    public function checkArr($arr) {
        $reg = '/[^0-9-]+|[0-9]+-|\s+|^$/';
        if (is_array($arr)) {
            foreach ($arr as $item) {
                preg_match_all($reg, $item, $matches);
                if(count($matches[0])) {
                    return false;
                }
            }
            return true;

        } else {
            return false;
        }
    }
}
