<?php

namespace ckeditor\Models;

use Illuminate\Database\Eloquent\Model;

class MusicType extends Model
{
    protected $table = 'music_types';

    public function singer()
    {
        return $this->belongsToMany('ckeditor\Models\Singers','singer_types', 'musicType_id', 'singer_id');
    }
}
