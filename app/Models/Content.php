<?php

namespace ckeditor\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'contents_table';
    protected $fillable = ['userId', 'content'];

}
