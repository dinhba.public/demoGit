<?php

namespace ckeditor\Models;

use Illuminate\Database\Eloquent\Model;

class Songs extends Model
{
    protected $table = 'songs';

    public function album()
    {
        return $this->belongsTo('ckeditor\Models\Albums', 'album_id', 'id');
    }

    public function composer()
    {
        return $this->belongsTo('ckeditor\Models\Singers', 'composer_id', 'id');
    }

    public function musicType()
    {
        return $this->belongsTo('ckeditor\Models\MusicType', 'musicType_id', 'id');
    }
}
