<?php

namespace ckeditor\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;
use ckeditor\Models\User;

class MemberController extends Controller
{
    /**
     * Get list member is activated
     *
     * @param Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $result = User::select('username', 'email', 'first_name', 'last_name')->where('activated', 1)->get();

        return view('admin.member.listmember')->with('result', $result);
    }

    public function addSinger(Request $request)
    {

    }
}
