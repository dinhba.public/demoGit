<?php

namespace ckeditor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;
use ckeditor\Models\Company;

class CompanyController extends Controller
{
    /**
     * Get list Company :
     *
     * @param Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $result = Company::select('name')->get();

        return view('admin.company.listCompany')->with('result', $result);
    }

    public function addCompany (Request $request)
    {
        $com = new Company();
        $com->name = 'Ben que Production';
        $result = $com->save();
        var_dump($result); die;


    }
}
