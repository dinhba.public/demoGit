<?php

namespace ckeditor\Http\Controllers\Front;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;

class VideosController extends Controller
{
    /**
     * return Video Page
     * @params : No params
     */
    public function index()
    {
        return view('front.pages.video');

    }
}
